
<!DOCTYPE html>
<html lang="en">
  <head>
      <base href="/public">
    @include('admin.css')
    <style type="text/css">
    label{
        display: inline-block;
        width: 200px;
    }
    </style>
  </head>
  <body>
    <div class="container-scroller">
      <!-- partial:partials/_sidebar.html -->
      @include('admin.sidebar')
      <!-- partial -->
      @include('admin.navbar')

      <div class="container-fluid page-body-wrapper">
          <div class="container" allign="center" style="padding: 100px">
            @if(session()->has('message'))

            <button type="button"class="close" data-dismiss="alert">
            </button>
    
            <div class="aler alert-success">
                {{session()->get('message')}}
            </div>
            @endif

            <form action="{{url('editdoctor', $data->id)}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div style="padding:15px;">
                    <label>Nama Docter</label>
                    <input type="text" style="color:black;" name="name" value="{{$data->name}}">
                </div>
                <div style="padding:15px;">
                    <label>Phone</label>
                    <input style="color:black; "type="text" name="phone" value="{{$data->phone}}">
                </div>
                <div style="padding:15px;">
                    <label>Speciality</label>
                    <input  style="color:black; "type="text" name="speciality" value="{{$data->speciality}}">
                </div>
                <div style="padding:15px;">
                    <label>Nomor Ruangan</label>
                    <input  style="color:black; "type="text" name="room" value="{{$data->room}}">
                </div>

                <div style="padding:15px;">
                    <label>Foto Lama</label>
                    <img height="150" width="150" src="doctorimage/{{$data->image}}">
                </div>
                <div style="padding: 15px;">
                    <label>Change Image</label>
                    <input type="file" name="file">
                </div>

                <div style="padding: 15px;">
                    
                    <input type="submit" class="btn btn-primary">
                </div>
            </form>
                
        
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
</div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    @include('admin.script')
    <!-- End custom js for this page -->
    <script>
        swal("Hello world!");
    </script>
  </body>

</html>