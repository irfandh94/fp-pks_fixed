
<!DOCTYPE html>
<html lang="en">
  <head>

    <style type="text/css">
    label{
        display: inline-block;
        width: 200px;
    }
    </style>
    @include('admin.css')
    
  </head>
  <body>
    <div class="container-scroller">
      <!-- partial:partials/_sidebar.html -->
      @include('admin.sidebar')
      <!-- partial -->
      @include('admin.navbar')
        
        <!-- main-panel ends -->
      <div class="container-fluid page-body-wrapper">
      

       

        <div class="container" allign="center" style="padding-top: 100px;">
            @if(session()->has('message'))

            <button type="button"class="close" data-dismiss="alert">
            </button>
    
            <div class="aler alert-success">
                {{session()->get('message')}}
            </div>
            @endif
            <form action="{{url('upload_berita')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div style="padding: 15px;">
                    <label>Judul Berita</label>
                    <input type="text" name="judul"  style="color:black;" placeholder="Ketik Judul Berita" required="">
                </div>

                <div style="padding: 15px;">
                    <label>deskripsi</label>
                    <input type="text" name="deskripsi"  style="color:black;" placeholder="Deskripsi" required="">
                </div>

                <div style="padding: 15px;">
                    <label>Sumber Berita</label>
                    <select name="sumberberita" style="color: black; 200px;" required="">
                        <option>--Select--</option>
                        <option value="detik.com">detik.com</option>
                        <option value="metrotv">metrotv</option>
                        <option value="silet">silet</option>
                        <option value="globaltv">globaltv</option>
                        <option value="cnnews">cnnews</option>
                    </select>

                </div>

                <div style="padding: 15px;">
                    <label>Nama Penulis</label>
                    <input type="text" name="penulis"  style="color:black;" placeholder="Masukkan Nomor Ruangan" required="">
                </div>

                <div style="padding: 15px;">
                    <label>Foto Berita</label>
                    <input type="file" name="file" required="">
                </div>

                <div style="padding: 15px;">
                    <input type="submit" class="btn btn-success">
                </div> 

            </form>
        </div>

      </div>
      <!-- page-body-wrapper ends -->
    
    <!-- container-scroller -->
    <!-- plugins:js -->
    @include('admin.script')
    <!-- End custom js for this page -->
  </body>
</html>