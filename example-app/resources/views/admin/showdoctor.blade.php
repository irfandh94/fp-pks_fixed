
<!DOCTYPE html>
<html lang="en">
  <head>
    @include('admin.css')
  </head>
  <body>
    <div class="container-scroller">
      <!-- partial:partials/_sidebar.html -->
      @include('admin.sidebar')
      <!-- partial -->
      @include('admin.navbar')
        
      <div class="container-fluid page-body-wrapper">

        <div align="center" style="padding-top:100px;">
        <table>
            <tr style="background-color: black; color:white;">
                <th style="padding: 10px">Nama Dokter</th>
                <th style="padding: 10px">Phone</th>
                <th style="padding: 10px">Speciality</th>
                <th style="padding: 10px">Room</th>
                <th style="padding: 10px">Image</th>
                <th style="padding: 10px">Delete</th>
                <th style="padding: 10px">Update</th>
            </tr>
            @foreach($data as $doctor)
            <tr align='center'style="background-color: black;">
                <td>{{$doctor->name}}</td>
                <td>{{$doctor->phone}}</td>
                <td>{{$doctor->speciality}}</td>
                <td>{{$doctor->room}}</td>
                <td><img height="100" width="300" src="doctorimage/{{$doctor->image}}"></td>
                <td> 
                    <a onclick="return confirm('apakah anda ingin menghapus data dokter ini')" class ="btn btn-danger"href="{{url('deletedoctor', $doctor->id)}}">Delete</a>
                </td>
                <td> 
                    <a class ="btn btn-primary" href="{{url('updatedoctor',$doctor->id)}}">Update</a>
                </td>
            </tr>
            @endforeach
        </table>
    </div>
</div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    <!-- container-scroller -->
    <!-- plugins:js -->
    @include('admin.script')
    <!-- End custom js for this page -->
  </body>
  <script>
   </script>
</html>