
<!DOCTYPE html>
<html lang="en">
  <head>

    <style type="text/css">
    label{
        display: inline-block;
        width: 200px;
    }
    </style>
    @include('admin.css')
    
  </head>
  <body>
    <div class="container-scroller">
      <!-- partial:partials/_sidebar.html -->
      @include('admin.sidebar')
      <!-- partial -->
      @include('admin.navbar')
        
        <!-- main-panel ends -->
      <div class="container-fluid page-body-wrapper">
      

       

        <div class="container" allign="center" style="padding-top: 100px;">
            @if(session()->has('message'))

            <button type="button"class="close" data-dismiss="alert">
            </button>
    
            <div class="aler alert-success">
                {{session()->get('message')}}
            </div>
            @endif
            <form action="{{url('upload_doctor')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div style="padding: 15px;">
                    <label>Nama Dokter</label>
                    <input type="text" name="name"  style="color:black;" placeholder="Ketik Nama Dokter" required="">
                </div>

                <div style="padding: 15px;">
                    <label>Phone</label>
                    <input type="number" name="number"  style="color:black;" placeholder="Ketik Nama Handphone" required="">
                </div>

                <div style="padding: 15px;">
                    <label>Speciality</label>
                    <select name="speciality" style="color: black; 200px;" required="">
                        <option>--Select--</option>
                        <option value="heart">Jantung</option>
                        <option value="eye">Mata</option>
                        <option value="umum">Umum</option>
                        <option value="tht">THT</option>
                        <option value="skin">Kulit</option>
                    </select>

                </div>

                <div style="padding: 15px;">
                    <label>Nomor Ruangan</label>
                    <input type="text" name="room"  style="color:black;" placeholder="Masukkan Nomor Ruangan" required="">
                </div>

                <div style="padding: 15px;">
                    <label>Foto Dokter</label>
                    <input type="file" name="file" required="">
                </div>

                <div style="padding: 15px;">
                    <input type="submit" class="btn btn-success">
                </div> 

            </form>
        </div>

      </div>
      <!-- page-body-wrapper ends -->
    
    <!-- container-scroller -->
    <!-- plugins:js -->
    @include('admin.script')
    <!-- End custom js for this page -->
  </body>
</html>