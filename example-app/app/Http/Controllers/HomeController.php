<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Doctor;
use App\Models\Appointment;
use App\Models\Berita;

class HomeController extends Controller
{
    public function redirect()
    {
        if(Auth::id())
        {
            if(Auth::user()->usertype=='0')
            {
                $doctor = doctor::all();
                $berita = berita::all();
                return view('user.home', compact(['doctor','berita']));
            }
            else
            {
                return view('admin.home');
            }
        }
        else{
            return redirect()->back();
        }
    }

    public function index()
    {   
        if(Auth::id())
        {
            return redirect('home');
        }
        else{
        $doctor = doctor::all();
        $berita = berita::all();
        return view('user.home',compact(['doctor','berita']));
        }
    }

    public function appointment(request $request)
    {
        $data = new appointment;
        $data->name=$request->name;
        $data->email=$request->email;
        $data->date=$request->date;
        $data->phone=$request->phone;
        $data->message=$request->message;
        $data->doctor=$request->doctor;
        $data->status='in progress';
        if(Auth::id())
        {
            $data->user_id=Auth::user()->id;
        }
        $data->save();

        return redirect()->back()->with('message', 'Permintaan Pertemuan telah disimpan. kami akan segera menghubungi anda');
    }
    public function myappointment()
    {   
        if(Auth::id())
        {   
            $userid=Auth::user()->id;
            $appoint=appointment::where('user_id',$userid)->get();
            return view('user.myappointment',compact('appoint'));
        }

        else {
            return redirect()->back();
        }
        
    }

    public function cancel_appoint($id)
    {
      $data=appointment::find($id);
      $data->delete();

      return redirect()->back();

    }
      
}
